<?php
$servername = "localhost";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
   }

if ($conn->select_db('notas') === false) {
   echo "database non existent\n";
   echo "trying to create database\n";
   $sql = "CREATE DATABASE notas";
   if ($conn->query($sql) === TRUE) {
      echo "BBDD created successfully";
   }else {
    echo "Error creating database: " . $conn->error;
}
   $_SESSION['connected']="up";
   $conn->select_db('notas');
     $sql = "CREATE TABLE notas (
     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
     note_name VARCHAR(50) NOT NULL,
     note VARCHAR(500) NOT NULL,
     favorite VARCHAR(1) DEFAULT 'n',
reg_date TIMESTAMP)";

   if (mysqli_query($conn, $sql)) {
      echo "Table created successfully";
   } else {
      echo "Error creating table: " . mysqli_error($conn);
}

}
   $conn->select_db('notas');

?>
